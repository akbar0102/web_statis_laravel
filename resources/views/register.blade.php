<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook - Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" name="fname" id="fname"><br>

        <p>Last name:</p>
        <input type="text" name="lname" id="lname"><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label>

        <p>Nationality:</p>
        <select id="nation" name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporen">Singaporen</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Thailand">Thailand</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="lang_1" name="bahasa" value="Bahasa Indonesia">
        <label for="lang_1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="lang_2" name="eng" value="English">
        <label for="lang_2">English</label><br>
        <input type="checkbox" id="lang_3" name="other" value="Other">
        <label for="lang_3">Other</label>

        <p>Bio:</p>
        <textarea name="bio" rows="10" cols="30"></textarea><br>

        <input type="submit" value="Sign Up">   
    </form>
</body>

</html>